/*
 This file contains ome example reviver functions. These functions search
 for a SSI style stamp in the block comments of a document. For example,
 placing the stamp <!--#test.txt--> in a document will cause stamp
 to be replaced by test.txt (if such a fragment is set inside Pider).
 The # mark is used to distinguish Pider stamps from regular comments.
 The javascript stamp is of the form /*#key*/
/**/

const xml_stamp_exp = new RegExp("<!--#" + "[^]+?" + "-->", "g");
const xml_key_exp = new RegExp("(?<=<!--#)" + "[^]+?" + "(?=-->)");
function xml_reviver(text) {
 const texts = text.split(xml_stamp_exp);
 const keys = [...text.matchAll(xml_stamp_exp)].map(stamp_match => {
  return xml_key_exp.exec(stamp_match)[0]
 });
 // most text editors will append an unwanted return character to files
 texts[texts.length - 1] = texts[texts.length - 1].trimEnd();
 return [texts, keys]
}

// works for javascript and css too
const java_stamp_exp = new RegExp("\/\\*#" + "[^]+?" + "\\*\/", "g");
const java_key_exp = new RegExp("(?<=\/\\*#)" + "[^]+?" + "(?=\\*\/)");
function java_reviver(text) {
 const texts = text.split(java_stamp_exp);
 const keys = [...text.matchAll(java_stamp_exp)].map(stamp_match => {
  return java_key_exp.exec(stamp_match)[0]
 });
 // most text editors will append an unwanted return character to files
 texts[texts.length - 1] = texts[texts.length - 1].trimEnd();
 return [texts, keys]
}

export {xml_reviver, java_reviver}
