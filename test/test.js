import {xml_reviver, java_reviver} from "../revivers.js";
import Pider from "../Pider.js";
import path from "path";

console.log("Running some simple test replacements.");
const test_html = "<p><!--#replace_me--></p>";
const replace_me = "I have been replaced.";
Pider.add("test_html", test_html, xml_reviver);
Pider.add("replace_me", replace_me, xml_reviver);
console.log(Pider.get("test_html"));

const test_java = 'String value = "\/*#replace_me*\/";';
Pider.add("test_java", test_java, java_reviver);
Pider.add("replace_me", replace_me, java_reviver);
console.log(Pider.get("test_java"));

if (path.basename(process.cwd()) == "Pider") {
 const file_tests = Promise.all([
  Pider.load("example.html", "test/example.html", xml_reviver),
  Pider.load("replace.html", "test/replace.html", xml_reviver),
  Pider.load("example2.html", "test/example2.html", xml_reviver),
  Pider.load("replace2.html", "test/replace2.html", xml_reviver),
  Pider.load("replace3.html", "test/replace3.html", xml_reviver)
 ]).then(results => {
  console.log(Pider.get("example.html"));
  console.log(Pider.get("example2.html"));
 });

 file_tests.then(() => {
  const backup = Pider.reset();
  console.log();
  console.log(
`Pider has been reset.\
 Only the example prompts have been reloaded.\
 Stamps should be converted to empty strings.`
  );
  Pider.add("test_html", test_html, xml_reviver);
  console.log(Pider.get("test_html"));
  Pider.add("test_java", test_java, java_reviver);
  console.log(Pider.get("test_java"));
  Promise.all([
   Pider.load("example.html", "test/example.html", xml_reviver),
   Pider.load("example2.html", "test/example2.html", xml_reviver)
  ]).then(results => {
   console.log(Pider.get("example.html"));
   console.log(Pider.get("example2.html"));
  }).then(() => {
   Pider.import(backup);
   console.log();
   console.log("This test will now reload from the original state.");
   console.log(Pider.get("test_html"));
   console.log(Pider.get("test_java"));
   console.log(Pider.get("example.html"));
   console.log(Pider.get("example2.html"));
  })
 });
}
else console.error(
 "The current working directory must be 'Pider' to run file based tests"
);

