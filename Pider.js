import * as fs from "node:fs/promises";

// A Map that initializes an object when get is called. Calling set modifies
// the object instead of changing the pointer (if the object is already set).
// This allows developers to dynamically link together objects.
class Memory extends Map {

 constructor(...args) {
  super(...args);
 }

 get(key) {
  if (!super.has(key)) super.set(key, new Pider());
  return super.get(key);
 }

 set(key, value) {
  if (super.has(key)) {
   const fragment = super.get(key);
   fragment.texts = value.texts;
   fragment.fragments = value.fragments;
  }
  else super.set(key, value);
 }

}

// thinking about having three distinct classes: Pider, Memory, and Fragment
/*
   The user will almost always only use the Pider static mehtods. The static
   methods allow a user to create SSI-style dynamic documents. The object
   part of Pider should probably be called a Fragment. A Fragment is a series
   of text strings with embeded Fragments. The root fragmetn is a text string
   with no embeded Fragments. Calling toString recursively parsed the Fragment
   into a string [text, fragment.toString(), text...]

   The reviver funstions are usually created by the user. They take in text,
   and output split it into [text, stamp, text, stamp, text...]. Pider then
   creates a Fragment by using the Pider constructor(texts, stamps). Note:
   all the texts are in the first array, all the stamps in the second. The
   number of stamps should always be 1 less than the number of texts (a stamp
   is always between two text nodes).
*/
class Pider {

 static #memory = new Memory();

 static reset() {
  const backup = this.#memory;
  this.#memory = new Memory();
  return backup;
 }

 static export() {
  return new Memory(this.#memory);
 }

 static import(memory) {
  const backup = this.#memory;
  this.#memory = new Memory(memory);
  return backup;
 }

 static get(key) {
  return this.#memory.get(key).toString();
 }

 static add(key, text, reviver) {
  let texts, keys;
  [texts, keys] = reviver(text);
  const fragment = new Pider(texts, keys);
  this.#memory.set(key, fragment);
  return fragment;
 }

 static async load(key, url, reviver) {
  const text = String(await fs.readFile(url));
  return this.add(key, text, reviver);
 }

 constructor(texts, keys) {
  if (typeof texts != "undefined") this.texts = texts;
  else this.texts = [''];
  if (typeof keys != "undefined") {
   this.fragments = keys.map(key => Pider.#memory.get(key));
  }
  else this.fragments = [];
 }

 toString() {
  return this.fragments.reduce((acc, fragment, i) => {
   return acc + fragment + this.texts[i+1];
  }, this.texts[0]);
 }

}

export default Pider
